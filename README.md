# Dependency

## Settings file

```
<settings>
  <servers>
    <server>
      <id>gitlab-maven</id>
      <configuration>
        <httpHeaders>
          <property>
            <name>Private-Token</name>
            <value>GitLab PAT Token</value>
          </property>
        </httpHeaders>
      </configuration>
    </server>
  </servers>
</settings>
```


## Uploading package

```shell
$ mvn deploy -s settings.xml
```
